
const getAllCars = () =>
  fetch('http://localhost:1250/cars')
    .then(res => res.json());


const getAllCars2 = async () => {
  const res = await fetch('http://localhost:1250/cars');
  const cars = await res.json();
  return cars;
};

getAllCars().then(cars => console.log(cars));

getAllCars2().then(cars => console.log(cars));
