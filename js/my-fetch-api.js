
// Create a function which implements the following API

const myFetch = (url, userOptions = {}) => {

  const options = {
    method: 'GET',
    headers: {},
    ...userOptions
  };

  console.log(options);

  return new Promise(resolve => {

    const xhr = new XMLHttpRequest();

    xhr.addEventListener('readystatechange', () => {
    
      if (xhr.status < 400 && xhr.readyState === 4) {
        resolve(JSON.parse(xhr.responseText));
      }
    
    });
    
    xhr.open(options.method, url);

    Object.keys(options.headers).forEach(key => {
      xhr.setRequestHeader(key, options.headers[key]);
    });

    if (options.body) {
      xhr.send(options.body);
    } else {
      xhr.send();
    }

  });

};

myFetch('http://localhost:1250/cars', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Accept-Type': 'application/json',
  },
  body: JSON.stringify({
    make: 'Chevrolet',
    model: 'Cruze',
    year: 2014,
  })
}).then(() => myFetch('http://localhost:1250/cars'))
  .then(cars => console.log(cars));

